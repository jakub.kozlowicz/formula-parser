import re
from collections import Counter

_ATOM_REGEX = r'([A-Z][a-z]*)(\d*)'
_OPENERS = r'({['
_CLOSERS = r')}]'


class Formula:
    def __init__(self, string):
        self.__formula = string


class ExpandFormula(Formula):
    def __init__(self, string):
        super().__init__(string)

    def __bracket_balanced(self):
        count = Counter(self._Formula__formula)
        return count['('] == count[')'] and count['['] == count[']'] and count['{'] == count['}']

    def __convert_to_dict(self, tuples):
        result = dict()
        for atom, n in tuples:
            try:
                result[atom] += int(n or 1)
            except KeyError:
                result[atom] = int(n or 1)

        return result

    def __fuse(self, mol1, mol2, w=1):
        result = dict()
        mol = set(mol1) | set(mol2)

        for atom in mol:
            result[atom] = (mol1.get(atom, 0) + mol2.get(atom, 0)) * w

        return result

    def __parse(self, formula):
        q = list()
        mol = dict()

        i = 0
        while i < len(formula):
            token = formula[i]

            if token in _CLOSERS:
                m = re.match(r'\d+', formula[i+1:])
                if m:
                    weight = int(m.group(0))
                    i += len(m.group(0))
                else:
                    weight = 1

                submol = self.__convert_to_dict(
                    re.findall(_ATOM_REGEX, ''.join(q)))

                return self.__fuse(mol, submol, weight), i

            elif token in _OPENERS:
                submol, l = self.__parse(formula[i+1:])
                mol = self.__fuse(mol, submol)

                i += l + 1
            else:
                q.append(token)

            i += 1

        return self.__fuse(mol, self.__convert_to_dict(re.findall(_ATOM_REGEX, ''.join(q)))), i

    def count_atoms(self) -> dict:
        if not self.__bracket_balanced():
            raise ValueError("Incomplete brackets!")

        return self.__parse(self._Formula__formula)[0]


class CountFormula(Formula):
    def __init__(self, string):
        super().__init__(string)

    def __parse(self):
        self._Formula__formula = re.sub(
            "[^a-zA-Z]+", "", self._Formula__formula)

        all_molecules = re.findall('[A-Z][^A-Z]*', self._Formula__formula)
        molecules = list()

        for molecule in all_molecules:
            if not molecule in molecules:
                molecules.append(molecule)

        return molecules

    def count_atoms(self) -> dict:
        result = dict()
        molecules = self.__parse()

        for molecule in molecules:
            count = self._Formula__formula.count(molecule)
            result[molecule] = count

        return result
