from formula import ExpandFormula, CountFormula


def main():
    print("Choose parser type")
    print(" [count] Count atoms from string")
    print(" [expand] Expand atoms from formula string")

    parser_type = input("Parser type: ")
    parse_formula = input("Type formula to parse: ")

    if parser_type.lower() == "expand":
        formula = ExpandFormula(parse_formula)
        result = formula.count_atoms()
        print(result)
    elif parser_type.lower() == "count":
        formula = CountFormula(parse_formula)
        result = formula.count_atoms()
        print(result)
    else:
        print("Unknown parser type")


if __name__ == '__main__':
    main()
