from formula import ExpandFormula, CountFormula


def test_count():
    FORMULAS = (
        ('$%H==@O)*H', {'H': 2, 'O': 1}),
        ('H)=Mg@=O)O*%H', {'H': 2, 'Mg': 1, 'O': 2}),
        ('C1C(=O)N(C2=C(C=C(C=C2)Br)C(=N1)C3=CC=CC=C3)CC(=O)NN',
         {'C': 17, 'O': 2, 'N': 4, 'Br': 1}),
        ('K3OP-(K#OKP=)KOK$PK*KPFeOK@KP(K(K%P=OPK)KP)K(OKP)',
         {'K': 15, 'P': 9, 'O': 6, 'Fe': 1}),
        ('(SO5(K=O5)ON)OO{SO(KO7N)O)SOKO2O)S}KO(O2)O',
         {'S': 4, 'K': 4, 'O': 14, 'N': 2})
    )

    for formula, result in FORMULAS:
        parser = CountFormula(formula)
        parsed_formula = parser.count_atoms()

    for atom in result.keys():
        assert result.get(atom, 0) == parsed_formula[atom]


def test_expand():
    FORMULAS = (
        ('H2O', {'H': 2, 'O': 1}),
        ('Mg(OH)2', {'H': 2, 'Mg': 1, 'O': 2}),
        ('K4[ON(SO3)2]2', {'S': 4, 'K': 4, 'O': 14, 'N': 2}),
        ('C2952H4664O832N812S8Fe4', {
         'N': 812, 'S': 8, 'H': 4664, 'C': 2952, 'O': 832, 'Fe': 4}),
        ('C43(H21He43[C6Hg999]{Si2[Na[Mo1]1]}42Au3)5{{D5}}', {
         'Si': 420, 'C': 73, 'Au': 15, 'Na': 210, 'H': 105, 'He': 215, 'Hg': 4995, 'Mo': 210, 'D': 5})
    )

    for formula, result in FORMULAS:
        parser = ExpandFormula(formula)
        parsed_formula = parser.count_atoms()

        for atom in result.keys():
            assert parsed_formula.get(atom, 0) == result[atom]
